<?php

defined('BASEPATH') or exit('No direct script access allowed');

class admin_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login($user, $password)
    {
        return $this->db->select('ad_name,ad_user,ad_status')->where(array('ad_user' => $user, 'ad_pass' => $password))->get('admin')->row_array();
    }

    public function get_user()
    {
        return $this->db->order_by('ad_id', 'ASC')->get('admin')->result_array();
    }
}
