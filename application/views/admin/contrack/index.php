<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-main ">-ข้อมูลบริษัท-</h6>
                        <div class="dropdown no-arrow">
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-area">
                            <div class="table-responsive">
                                <table class="table " id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>ข้อมูลเกี่ยวกับบริษัท</th>
                                            <th>ที่อยู่</th>
                                            <th>อีเมล์</th>
                                            <th>เบอร์โทรติดต่อ</th>
                                            <th>ไอดีไลน์</th>
                                            <th>QR Line</th>
                                            <th>facebook</th>
                                            <th>website</th>
                                            <th>รูปแผนที่</th>
                                            <th>แผนที่ google map</th>
                                            <th>วันเวลาทำการ</th>
                                            <th>โลโก้บริษัท</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>11111111111111111111112</td>
                                            <td>137777777777777777772</td>
                                            <td>13999999999999992</td>
                                            <td>1165161561461561532</td>
                                            <td>16688888888888888832</td>
                                            <td>13666666666666666662</td>
                                            <td>1361561561561561562</td>
                                            <td>134455555555555555555552</td>
                                            <td>134444444444444442</td>
                                            <td>13849489549654949452</td>
                                            <td>1316546864894898492</td>
                                            <td>1111111132</td>
                                            <td>13111111111111112</td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ml-3" id="exampleModalLongTitle">เพิ่มข้อมูลบริษัท</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>ข้อมูลบริษัท</label>
                                <input type="text" id="about" name="about" class="form-control" value="ข้อมูลบริษัท" placeholder="ข้อมูลบริษัท" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>



                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>อีเมล์</label>
                                <input type="email" id="email" name="email" class="form-control" value="อีเมล์" placeholder="อีเมล์" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>เบอร์โทรติดต่อ</label>
                                <input type="tel" id="phone" name="phone" class="form-control" value="061646" placeholder="เบอร์โทรติดต่อ" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label>ไอดีไลน์</label>
                                <input type="text" id="idLine" name="idLine" class="form-control" value="" placeholder="ไอดีไลน์" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>คิวอาร์โค้ดwไลน์</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">เลือกไฟล์</label>
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>ลิ้งค์ Facebook</label>
                                <input type="text" id="linkfb" name="linkfb" class="form-control" value="" placeholder="ลิ้งค์ facebook" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>ลิ้งค์ Website</label>
                                <input type="text" id="linkWeb" name="linkWeb" class="form-control" value="" placeholder="ลิ้งค์ Website" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>รูปแผนที่</label>
                                <input type="text" id="photoMap" name="photoMap" class="form-control" value="" placeholder="รูปแผนที่" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>แผนที่ google map</label>
                                <input type="text" id="locationMap" name="locationMap" class="form-control" value="" placeholder="แผนที่ google map" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>เวลาทำการ</label>
                                <input type="text" id="time" name="time" class="form-control" value="" placeholder="เวลาทำการ" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>โลโก้บริษัท</label>
                                <input type="text" id="logoBusiness" name="logoBusiness" class="form-control" value="" placeholder="โลโก้บริษัท" />
                            </div>
                        </div>
                        <div class="col-md-2"></div>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>ที่อยู่</label>
                                <textarea id="textEditor" name="address" class="form-control"></textarea>
                            </div>
                            <div class="col-md-2"></div>

                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">ยืนยัน</button>
            </div>
        </div>
    </div>
</div>