<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-main ">-ข้อมูลบริการ-</h6>
                        <div class="dropdown no-arrow">
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-area">
                            <table class="table " id="dataTable">
                                <thead>
                                    <tr>
                                        <th>หัวข้อบริการ</th>
                                        <th>เนื้อหา</th>
                                        <th>เนื้อหาย่อย</th>
                                        <th>รูปภาพ</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>132</td>
                                        <td>132</td>
                                        <td>132</td>
                                        <td>132</td>
                                        <td>132</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title ml-3" id="exampleModalLongTitle">เพิ่มข้อมูลสินค้า</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>หัวข้อบริการ</label>
                                        <input type="text" id="titleService" name="titleService" class="form-control" value="" placeholder="หัวข้อบริการ" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>เนื้อหาหลัก</label>
                                        <textarea id="textEditor" name="detailService" class="form-control"></textarea>

                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>เนื้อหาย่อย</label>
                                        <textarea id="textEditor" name="subDetailService" class="form-control"></textarea>

                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>รูปภาพ</label>
                                        <input type="text" id="imgService" name="imgService" class="form-control" value="" placeholder="รูปภาพ" />
                                        
                                    </div>
                                    <div class="col-md-2"></div>

                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">ยืนยัน</button>
                    </div>
                </div>
            </div>
        </div>