<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('admin/admin_model', 'admin');
		if (empty($this->session->userdata('user'))) {
			redirect(base_url() . 'auth');
		}
	}
	public function index()
	{
		$data['get_user'] =  $this->admin->get_user();
		$this->load->view('layouts_admin/_header');
		$this->load->view('admin/user/index', $data);
		$this->load->view('layouts_admin/_footer');
	}


	public function create_admin()
	{
		// $this->load->view('layouts_admin/_header');
		// $this->load->view('admin/user/create_modal');
		// $this->load->view('layouts_admin/_footer');
	}


}
