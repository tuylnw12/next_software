<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Slide extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('admin/admin_model', 'admin');
		if (empty($this->session->userdata('user'))) {
			redirect(base_url() . 'auth');
		}
	}
	public function index()
	{
		$this->load->view('layouts_admin/_header');
		$this->load->view('admin/slide/index');
		$this->load->view('layouts_admin/_footer');
	}



}